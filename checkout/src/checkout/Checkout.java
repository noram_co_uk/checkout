package checkout;

import static java.lang.System.out;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;


public class Checkout {
	
	static final Map<Integer, Integer> PRODUCT_PRICES = new HashMap<>();		// Product, Price (pence)
	static {
		PRODUCT_PRICES.put(1, 60);			// Apples
		PRODUCT_PRICES.put(2, 25);			// Oranges
	}
	
	static Function<CountPrice, Long> OFFER_BOGOF = new Function<CountPrice, Long>() {
		@Override
		public Long apply(CountPrice t) {
			return Math.floorDiv(t.count, 2) * t.price + (t.count % 2) * t.price;
		}
	};
    
	static Function<CountPrice, Long> OFFER_THREEFORTWO = new Function<CountPrice, Long>() {
		@Override
		public Long apply(CountPrice t) {
			return Math.floorDiv(t.count, 3) * 2 * t.price + (t.count % 3) * t.price;
		}
	};

	static Function<CountPrice, Long> OFFER_NONE = new Function<CountPrice, Long>() {
		@Override
		public Long apply(CountPrice t) {
			return t.count * t.price;
		}
	};

	static final Map<Integer, Function<CountPrice, Long>> OFFERS = new HashMap<>();	// Product, Offer function
	static {
		OFFERS.put(1, OFFER_BOGOF);
		OFFERS.put(2, OFFER_THREEFORTWO);
	}
	
	static final DecimalFormat FORMATTER = new DecimalFormat("  Total �###,##0.00");

	public static void main(String args []) {
		runTest("Test 1 [Apple] => �0.60:", Arrays.asList(1));
		runTest("Test 1 [Apple, Apple] => �0.60:", Arrays.asList(1,1));
		runTest("Test 1 [Apple, Apple, Apple] => �1.20:", Arrays.asList(1,1,1));
		runTest("Test 1 [Apple, Apple, Apple, Apple] => �1.20:", Arrays.asList(1,1,1,1));
		runTest("Test 1 [Orange] => �0.25:", Arrays.asList(2));
		runTest("Test 1 [Orange, Orange] => �0.50:", Arrays.asList(2,2));
		runTest("Test 1 [Orange, Orange, Orange] => �0.50:", Arrays.asList(2,2,2));
		runTest("Test 1 [Orange, Orange, Orange, Orange] => �0.75:", Arrays.asList(2,2,2,2));
		runTest("Test 1 [Orange, Orange, Orange, Orange, Orange] => �1.00:", Arrays.asList(2,2,2,2,2));
		runTest("Test 1 [Orange, Orange, Orange, Orange, Orange, Orange] => �1.00:", Arrays.asList(2,2,2,2,2,2));
		runTest("Test 5 (Empty list) => �0.00:", Arrays.asList());
		runTest("Test 6 (Null list) => �0.00:", null);
	}
	
	static void runTest(final String description, final List<Integer> items) {
		try {
			out.println(description);
			out.println(FORMATTER.format(Checkout.getTotalPrice(items)));
		} catch (IllegalArgumentException e) {
			out.println(e);
		}
	}

	static double getTotalPrice(List<Integer> products) {
		if (products == null || products.isEmpty()) {
			return 0;
		}
		
		if (products.stream().anyMatch(product -> !PRODUCT_PRICES.containsKey(product))) {
			throw new IllegalArgumentException("Error: Unpriced product(s) found.");
		}
		
		final Map<Integer, Long> productCount = products.stream()
				.distinct()
				.collect(toMap(
						item -> item,
						item -> products.stream().filter(i -> i.equals(item)).count()));
		
		final long total = productCount.keySet().stream()
			.mapToLong(product -> getPricer(product)
					.apply(new CountPrice(productCount.get(product), PRODUCT_PRICES.get(product))))
			.sum();
		
		return (double)total/100;			// Pence to Pounds
	}
	
	static Function<CountPrice, Long> getPricer(Integer product) {
		return OFFERS.containsKey(product) ? OFFERS.get(product) : OFFER_NONE;
	}
}

class CountPrice {
	long count;
	int price;
	
	CountPrice(final long count, final int price) {
		this.count = count;
		this.price = price;
	}
}